import { useNavigate, useRoutes } from "react-router-dom";
import { MainPage } from "./pages/MainPage";
import { TestPage } from "./pages/TestPage";
import Layout, { Wrapper } from "./layout/Layout";
import { LoginPage, RegisterPage } from "./pages";
import { PATHS } from "./util/errorCode";
import { useEffect } from "react";

const withLayout = (element: React.ReactNode) => <Layout>{element}</Layout>;

export default function Router() {
  const navigate = useNavigate();

  useEffect(() => {
    const token = localStorage.getItem("token");
    console.log("token : ", token);
    if (!token) {
      navigate(PATHS.login);
    }
  }, []);

  return useRoutes([
    {
      path: PATHS.main,
      element: withLayout(<MainPage />),
      children: [
        {
          path: PATHS.userList,
        },
        {
          path: PATHS.mailHistory,
        },
        {
          path: PATHS.domain,
        },
        {
          path: PATHS.client,
        },
        {
          path: PATHS.mailServiceProvider,
        },
      ],
    },
    {
      path: PATHS.login,
      element: (
        <Wrapper>
          <LoginPage />
        </Wrapper>
      ),
    },
    {
      path: PATHS.register,
      element: (
        <Wrapper>
          <RegisterPage />
        </Wrapper>
      ),
    },
    {
      path: PATHS.component,
      element: withLayout(<TestPage />),
    },
    {
      path: PATHS.notFound,
      element: <div>404 NOT FOUND</div>,
    },
  ]);
}
