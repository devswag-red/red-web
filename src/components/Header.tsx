import styled from "@emotion/styled";
import Logo from "../assets/RED.svg";
import Raccoon from "../assets/raccoon.png";
import { Flex } from "@radix-ui/themes";
import { useNavigate } from "react-router-dom";

export const Header = () => {
  const navigate = useNavigate();

  return (
    <HeaderContainer>
      <LogoBox>
        <img
          src={Logo}
          alt="Logo"
          width={88}
          height={39}
          style={{ marginTop: "13px", marginLeft: "1.5px" }}
        />
      </LogoBox>
      <ProfileBox>
        <Flex align={"center"} style={{ gap: "20px" }}>
          <Flex gap={"4"} align={"center"}>
            <img
              src={Raccoon}
              width={48}
              height={48}
              style={{ borderRadius: "50%" }}
            />
            <div>Admin</div>
          </Flex>
          <button
            style={{
              background: "white",
              color: "#FF5533",
              border: "1px solid #FF5533",
              width: "94px",
              height: "44px",
              borderRadius: "6px",
            }}
            onClick={() => {
              localStorage.removeItem("token");
              navigate("/login");
            }}
          >
            로그아웃
          </button>
        </Flex>
      </ProfileBox>
    </HeaderContainer>
  );
};

const HeaderContainer = styled.div`
  height: 90px;
  /* gap: 33.5px; */
  background-color: transparent;
  box-sizing: border-box;
  justify-content: space-between;
  display: flex;
`;

const LogoBox = styled.div`
  width: 318px;
  height: 90px;
  margin-right: 20px;
  border-bottom: 0.3px solid rgba(64, 70, 79, 0.3);
`;

const ProfileBox = styled.div`
  border-radius: 24px;
  width: 318px;
  background-color: #fff;
  display: flex;
  justify-content: right;
  padding-right: 40px;
  height: 90px;

  /* width: 81%; */
  flex: 1;
`;
