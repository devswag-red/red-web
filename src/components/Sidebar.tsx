import styled from "@emotion/styled";
import { Button } from "@radix-ui/themes";
import { ISideMenu } from "../types/types";
import { useNavigate } from "react-router-dom";

interface IProps {
  menuList: ISideMenu[];
}

export default function Sidebar({ menuList }: IProps) {
  const navigate = useNavigate();
  console.log(window.location.pathname);

  return (
    <SidebarContainer>
      {menuList.length > 0 &&
        menuList.map((menu) => {
          const Icon = menu.icon;
          const isActive = window.location.pathname === menu.path;
          console.log(":isActive", isActive);
          return (
            <StyledButton
              key={menu.id}
              onClick={() => navigate(menu.path)}
              $active={isActive}
            >
              <Icon
                key={menu.id}
                fill={isActive ? "#F53217" : "rgba(101, 112, 129, 1)"}
              />
              {menu.title}
            </StyledButton>
          );
        })}
    </SidebarContainer>
  );
}

const SidebarContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 318px;
  height: 100%;
  gap: 12px;
  margin-right: 20px;
`;

const StyledButton = styled(Button)<{ $active: boolean }>`
  display: flex;
  padding: 10px 12px;
  width: 100%;
  align-items: center;
  justify-content: left;
  gap: 24px;
  height: 4.69%;
  border-radius: 8px;
  background: ${(props) => (props.$active ? "#eaecf0" : "transparent")};
  cursor: pointer;
  min-height: 44px;

  color: ${(props) => (props.$active ? "#F53217" : "rgba(101, 112, 129, 1)")};
  font-feature-settings:
    "salt" on,
    "liga" off;
  font-family: Pretendard;
  font-size: 16px;
  font-style: normal;
  font-weight: 500;
  line-height: 100%;

  :hover {
    background: #eaecf0;
  }
`;
