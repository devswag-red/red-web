import styled from "@emotion/styled";
import { Root, Label } from "@radix-ui/react-form";

const RegisterText = styled.p`
  color: #657081;
  text-align: center;
  font-feature-settings: "liga" off, "clig" off;
  font-family: Pretendard;
  font-size: 16px;
  font-style: normal;
  font-weight: 500;
  line-height: 24px;
  margin-top: 8px;
  cursor: pointer;
`;

const FormWapper = styled(Root)`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 64px;
`;

const TextLabel = styled(Label)`
  color: #4b5267;
  font-feature-settings: "liga" off, "clig" off;
  font-family: Pretendard;
  font-size: 14px;
  font-style: normal;
  font-weight: 400;
  line-height: 20px;
  margin-bottom: 8px;
`;

const LoginInput = styled.input`
  display: flex;
  width: 100%;
  height: 48px;
  padding: 12px 16px;
  align-items: flex-start;
  gap: 10px;
  align-self: stretch;
  border-radius: 4;
  opacity: 1;
  background: #f5f6f8;
  box-sizing: border-box;
  border: none;

  color: #a0aabb;
  font-feature-settings: "liga" off, "clig" off;
  font-family: Pretendard;
  font-size: 16px;
  font-style: normal;
  font-weight: 400;
  line-height: 24px;
`;

const SubmitButton = styled.button`
  display: flex;
  padding: 12px 24px;
  width: 400px;
  height: 48px;
  justify-content: center;
  border: none;
  align-items: center;
  gap: 10px;
  flex-shrink: 0;
  color: #fff;
  text-align: center;
  font-feature-settings: "liga" off, "clig" off;
  border-radius: 4px;
  opacity: 1;
  background: #f53217;
  font-family: Pretendard;
  font-size: 16px;
  font-style: normal;
  font-weight: 500;
  line-height: 24px;
`;

export { SubmitButton, LoginInput, RegisterText, TextLabel, FormWapper };
