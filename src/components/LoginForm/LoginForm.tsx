import { Field, Message, Control, Submit } from "@radix-ui/react-form";
import * as S from "./loginformStyle";
import Logo from "../../assets/RED.svg";
import { useNavigate } from "react-router-dom";
import { FormEvent, useState } from "react";
import { ERROR_CODE, PATHS } from "../../util/errorCode";
import { Authenticate } from "../../api/Authenticate";

const matchErrorMsg = (error: string) => {
  switch (error) {
    case ERROR_CODE.INVALID_CREDENTIALS:
      return "아디디 혹은 패스워드가 잘못되었습니다.";
    default:
  }
};

const LoginForm = () => {
  const [serverError, setServerError] = useState("");
  const navigate = useNavigate();
  const loginApi = Authenticate.getInstance();

  const onSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const { id, password } = Object.fromEntries(new FormData(e.currentTarget));

    if (!id || !password) {
      setServerError(ERROR_CODE.INVALID_CREDENTIALS);
      return;
    }

    const { data } = await loginApi.logIn({
      username: id as string,
      password: password as string,
    });
    console.log(data);
    localStorage.setItem("token", JSON.stringify(data).replaceAll('"', ""));
    navigate(PATHS.main, { replace: true });
  };

  return (
    <S.FormWapper style={{ width: "100%" }} onSubmit={onSubmit}>
      <img
        src={Logo}
        alt="Logo"
        width={176}
        height={77}
        style={{ marginBottom: "36px" }}
      />
      <Field name="id" style={{ width: "100%" }}>
        <div
          style={{
            display: "flex",
            alignItems: "baseline",
            justifyContent: "space-between",
          }}
        >
          <S.TextLabel>아이디</S.TextLabel>
          <Message match="valueMissing" style={{ color: "red", fontSize: 13 }}>
            아이디를 입력해 주세요
          </Message>
          {serverError && (
            <Message style={{ color: "red", fontSize: 13 }}>
              {matchErrorMsg(serverError)}
            </Message>
          )}
        </div>
        <Control asChild>
          <S.LoginInput type="id" required placeholder="아이디 입력" />
        </Control>
      </Field>
      <Field name="password" style={{ width: "100%", marginTop: "16px" }}>
        <div
          style={{
            display: "flex",
            alignItems: "baseline",
            justifyContent: "space-between",
          }}
        >
          <S.TextLabel>비밀번호</S.TextLabel>
          <Message match="valueMissing" style={{ color: "red", fontSize: 13 }}>
            비밀번호를 입력해 주세요
          </Message>
          {serverError && (
            <Message style={{ color: "red", fontSize: 13 }}>
              {matchErrorMsg(serverError)}
            </Message>
          )}
        </div>
        <Control asChild>
          <S.LoginInput
            type="password"
            required
            placeholder="비밀번호 입력
        "
          />
        </Control>
      </Field>
      <Submit asChild>
        <S.SubmitButton style={{ marginTop: 48 }}>로그인</S.SubmitButton>
      </Submit>
    </S.FormWapper>
  );
};

export default LoginForm;
