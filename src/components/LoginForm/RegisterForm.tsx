import { Field, Message, Control, Submit } from "@radix-ui/react-form";
import * as S from "./loginformStyle";
import Logo from "../../assets/RED.svg";
import { useNavigate } from "react-router-dom";
import { FormEvent, useState } from "react";
import { ERROR_CODE, PATHS } from "../../util/errorCode";

const matchErrorMsg = (error: string) => {
  switch (error) {
    case ERROR_CODE.NOT_MATCH_PASSWORD:
      return "비밀번호가 일치하지 않습니다.";
    default:
  }
};

const RegisterForm = () => {
  const [serverErrors, setServerErrors] = useState<{
    id: string;
    password: string;
  }>({
    id: "",
    password: "",
  });

  const navigate = useNavigate();

  const onSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const { id, password, passwordConfirm } = Object.fromEntries(
      new FormData(e.currentTarget),
    );

    if (password !== passwordConfirm) {
      return setServerErrors((prev) => {
        return { ...prev, password: ERROR_CODE.NOT_MATCH_PASSWORD };
      });
    }
    sessionStorage.setItem("account", JSON.stringify({ id, password }));
    navigate(PATHS.login);
  };

  return (
    <S.FormWapper style={{ width: "100%" }} onSubmit={onSubmit}>
      <img
        src={Logo}
        alt="Logo"
        width={176}
        height={77}
        style={{ marginBottom: "36px" }}
      />
      <Field name="id" style={{ width: "100%" }}>
        <div
          style={{
            display: "flex",
            alignItems: "baseline",
            justifyContent: "space-between",
          }}
        >
          <S.TextLabel>아이디</S.TextLabel>
          <Message match="valueMissing" style={{ color: "red", fontSize: 13 }}>
            아이디를 입력해 주세요
          </Message>
        </div>
        <Control asChild>
          <S.LoginInput type="id" required placeholder="아이디 입력" />
        </Control>
      </Field>
      <Field name="password" style={{ width: "100%", marginTop: "16px" }}>
        <div
          style={{
            display: "flex",
            alignItems: "baseline",
            justifyContent: "space-between",
          }}
        >
          <S.TextLabel>비밀번호</S.TextLabel>
          <Message match="valueMissing" style={{ color: "red", fontSize: 13 }}>
            비밀번호를 입력해 주세요
          </Message>
          {serverErrors.password && (
            <Message style={{ color: "red", fontSize: 13 }}>
              {matchErrorMsg(serverErrors.password as keyof typeof ERROR_CODE)}
            </Message>
          )}
        </div>
        <Control asChild>
          <S.LoginInput
            type="password"
            required
            placeholder="비밀번호 입력
        "
          />
        </Control>
      </Field>
      <Field
        name="passwordConfirm"
        style={{ width: "100%", marginTop: "16px" }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "baseline",
            justifyContent: "space-between",
          }}
        >
          <S.TextLabel>비밀번호 확인</S.TextLabel>
          <Message match="valueMissing" style={{ color: "red", fontSize: 13 }}>
            비밀번호를 입력해 주세요
          </Message>
          {serverErrors.password && (
            <Message style={{ color: "red", fontSize: 13 }}>
              {matchErrorMsg(serverErrors.password as keyof typeof ERROR_CODE)}
            </Message>
          )}
        </div>
        <Control asChild>
          <S.LoginInput
            type="password"
            required
            placeholder="비밀번호 입력
        "
          />
        </Control>
      </Field>
      <Submit asChild>
        <S.SubmitButton style={{ marginTop: 48 }}>가입하기</S.SubmitButton>
      </Submit>
    </S.FormWapper>
  );
};

export default RegisterForm;
