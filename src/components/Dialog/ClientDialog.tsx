import { Cross2Icon } from "@radix-ui/react-icons";
import {
  Flex,
  Text,
  Dialog,
  TextField,
  Button,
  RadioGroup,
  IconButton,
  Select,
} from "@radix-ui/themes";
import { useEffect, useState } from "react";
import { Client } from "../../api/Client";
import {
  ClientCreateRequest,
  ClientUpdateRequest,
} from "../../api/data-contracts";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { Domain } from "../../api/Domain";

export default function ClientDialog({
  type,
  id,
  setId,
}: {
  type: "update" | "create";
  id: string;
  setId?: React.Dispatch<React.SetStateAction<string>>;
}) {
  const { detail1, save2, update2 } = new Client();
  const { content } = new Domain();
  const { data } = useQuery({
    queryKey: ["clientProvider", id],
    queryFn: async () => (await detail1(id)).data,
    enabled: !!id,
    refetchOnMount: true,
    gcTime: 0,
  });
  const queryClient = useQueryClient();

  const { data: domainData } = useQuery({
    queryKey: ["domain"],
    queryFn: async () => (await content({ target: "ID" })).data,
    staleTime: 5000,
  });
  const [clientForm, setClientForm] = useState<
    ClientCreateRequest & ClientUpdateRequest
  >({ enabled: true, domainId: domainData?.[0]?.id });
  useEffect(() => {
    if (data) {
      setClientForm(data);
    }
  }, [data]);
  useEffect(() => {
    setClientForm((prev) => ({
      ...prev,
      domainId: domainData?.[0].id,
    }));
  }, [domainData]);

  const mutate = useMutation({
    mutationFn: ({ id, clientForm }: any) =>
      update2(id, {
        domainId: clientForm.domainId,
        enabled: clientForm.enabled,
      }),
    onSuccess: () => {
      queryClient.invalidateQueries(); // 도메인 목록을 갱신하기 위해 invalidate
    },
  });

  const mutateSave = useMutation({
    mutationFn: (clientForm: any) => save2(clientForm),
    onSuccess: () => {
      queryClient.invalidateQueries(); // 도메인 목록을 갱신하기 위해 invalidate
    },
  });

  const submit = () => {
    if (type === "create") {
      mutateSave.mutate(clientForm);
    } else {
      mutate.mutate({ id, clientForm });
    }
  };
  return (
    <Dialog.Content
      style={{ padding: "44px 42px", width: "492px" }}
      onCloseAutoFocus={() => {
        setId && setId("");
      }}
    >
      <Flex justify={"end"}>
        <Dialog.Close>
          <IconButton variant="ghost" radius="full" color="gray">
            <Cross2Icon width="18" height="18" fill="black" />
          </IconButton>
        </Dialog.Close>
      </Flex>
      <Dialog.Title>
        {type === "create" ? "클라이언트 생성" : "클라이언트 상세"}
      </Dialog.Title>

      <Flex direction="column" gap="4">
        {type === "update" && (
          <label>
            <Text as="div" size="1" mb="1" weight="medium">
              ID
            </Text>
            <TextField.Root
              placeholder="아이디 입력"
              defaultValue={data?.domainId}
              style={{
                width: "408px",
                height: "48px",
                background: "#F5F6F8",
                padding: "16px",
              }}
              disabled={true}
            />
          </label>
        )}
        <label>
          <Text as="div" size="1" mb="1" weight="medium">
            도메인
          </Text>
          <Flex direction="column" width="408px">
            {type === "update" ? (
              data?.domainId && (
                <Select.Root
                  defaultValue={data?.domainId}
                  size="3"
                  onValueChange={(value) => {
                    setClientForm((prev) => ({
                      ...prev,
                      domainId: value,
                    }));
                  }}
                >
                  <Select.Trigger radius="medium" />
                  <Select.Content>
                    {domainData?.map((domain) => (
                      <Select.Item value={domain.id ?? ""}>
                        {domain.name}
                      </Select.Item>
                    ))}
                  </Select.Content>
                </Select.Root>
              )
            ) : (
              <Select.Root
                defaultValue={domainData?.[0]?.id}
                size="3"
                onValueChange={(value) => {
                  setClientForm((prev) => ({
                    ...prev,
                    domainId: value,
                  }));
                }}
              >
                <Select.Trigger radius="medium" />
                <Select.Content>
                  {domainData?.map((domain) => (
                    <Select.Item value={domain.id ?? ""}>
                      {domain.name}
                    </Select.Item>
                  ))}
                </Select.Content>
              </Select.Root>
            )}
          </Flex>
        </label>
        {type === "update" && (
          <label>
            <Text as="div" size="1" mb="1" weight="medium">
              secret
            </Text>
            <TextField.Root
              defaultValue={data?.secret}
              placeholder="secret 입력"
              style={{
                width: "408px",
                height: "48px",
                background: "#F5F6F8",
                padding: "16px",
              }}
              disabled={true}
            />
          </label>
        )}
        <label>
          <Text as="div" size="1" mb="1" weight="medium">
            활성상태
          </Text>
          {
            <RadioGroup.Root
              value={clientForm?.enabled ? "active" : "inactive"}
              name="활성상태"
              color="tomato"
              onValueChange={(value) =>
                setClientForm((prev) => ({
                  ...prev,
                  enabled: value === "active",
                }))
              }
            >
              <Flex direction="row" style={{ gap: "136px" }}>
                <RadioGroup.Item value="active">활성</RadioGroup.Item>
                <RadioGroup.Item value="inactive">비활성</RadioGroup.Item>
              </Flex>
            </RadioGroup.Root>
          }
        </label>
      </Flex>

      <Flex gap="3" mt="7" justify="center">
        <Dialog.Close>
          <Button
            onClick={submit}
            style={{
              background: "#F53217",
              padding: "8px 16px 8px 16px",
              width: "100px",
              height: "40px",
            }}
          >
            저장
          </Button>
        </Dialog.Close>
      </Flex>
    </Dialog.Content>
  );
}
