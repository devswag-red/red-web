import { Cross2Icon } from "@radix-ui/react-icons";
import {
  Flex,
  Text,
  Dialog,
  TextField,
  Button,
  IconButton,
} from "@radix-ui/themes";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { Domain } from "../../api/Domain";
import { useEffect, useState } from "react";

export default function DomainDialog({
  type,
  id,
}: {
  type: "update" | "create";
  id?: string;
}) {
  const queryClient = useQueryClient();
  const { detail, save1, update1 } = Domain.getInstance();

  const [name, setName] = useState<string>("");

  const { data, isLoading } = useQuery({
    queryKey: ["GET_DOMAIN_DETAIL", id],
    queryFn: async () => (await detail(id as string)).data,
    enabled: !!id,
  });

  useEffect(() => {
    if (data && !isLoading) {
      setName(data.name as string);
    }
  }, [data, isLoading]);

  const saveMutation = useMutation({
    mutationFn: (newDomain: { name: string }) => save1(newDomain),
    onSuccess: () => {
      queryClient.invalidateQueries(); // 도메인 목록을 갱신하기 위해 invalidate
    },
  });

  const updateMutation = useMutation({
    mutationFn: (updatedDomain: { id: string; name: string }) =>
      update1(updatedDomain.id, { name: updatedDomain.name }),
    onSuccess: () => {
      queryClient.invalidateQueries();
    },
  });

  const handleSubmit = () => {
    if (type === "create") {
      saveMutation.mutate({ name });
    } else if (type === "update" && id) {
      updateMutation.mutate({ id, name });
    }
  };

  if (isLoading) {
    return <>로딩</>;
  }

  return (
    <Dialog.Content style={{ padding: "44px 42px", width: "492px" }}>
      <Flex justify={"end"}>
        <Dialog.Close>
          <IconButton variant="ghost" radius="full" color="gray">
            <Cross2Icon width="18" height="18" fill="black" />
          </IconButton>
        </Dialog.Close>
      </Flex>
      <Dialog.Title>
        {type === "create" ? "도메인 생성" : "도메인 수정"}
      </Dialog.Title>
      <Flex direction="column" gap="3">
        {type === "update" && (
          <label>
            <Text as="div" size="1" mb="1" weight="medium">
              ID
            </Text>
            <TextField.Root
              disabled={true}
              placeholder="이름 입력"
              style={{
                width: "408px",
                height: "48px",
                background: "#DBDFE5",
                padding: "16px",
              }}
              defaultValue={data?.id || ""}
            />
          </label>
        )}
        <label>
          <Text as="div" size="1" mb="1" weight="medium">
            이름
          </Text>
          <TextField.Root
            placeholder="이름 입력"
            style={{
              width: "408px",
              height: "48px",
              background: "#F5F6F8",
              padding: "16px",
            }}
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </label>
      </Flex>

      <Flex gap="3" mt="7" justify="center">
        <Dialog.Close>
          <Button
            style={{
              background: "#F53217",
              padding: "8px 16px 8px 16px",
              width: "100px",
              height: "40px",
            }}
            onClick={handleSubmit}
            disabled={saveMutation.isPending || updateMutation.isPending}
          >
            저장
          </Button>
        </Dialog.Close>
        <Dialog.Close>
          <Button
            style={{
              background: "#DBDFE5",
              padding: "8px 16px 8px 16px",
              width: "100px",
              height: "40px",
            }}
          >
            취소
          </Button>
        </Dialog.Close>
      </Flex>
    </Dialog.Content>
  );
}
