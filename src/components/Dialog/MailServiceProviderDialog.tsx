import { Cross2Icon } from "@radix-ui/react-icons";
import {
  Flex,
  Text,
  Dialog,
  TextField,
  Button,
  RadioGroup,
  IconButton,
  Select,
} from "@radix-ui/themes";
import { useQuery, useQueryClient } from "@tanstack/react-query";
import { MailServiceProvider } from "../../api/MailServiceProvider";
import { useEffect, useState } from "react";
import { MailServiceProviderRequest } from "../../api/data-contracts";
import { Domain } from "../../api/Domain";

export default function MailServiceProviderDialog({
  type,
  id,
  setId,
}: {
  id: string;
  type: "update" | "create";
  setId?: React.Dispatch<React.SetStateAction<string>>;
}) {
  const { update, getDetail, save } = new MailServiceProvider();
  const [mailForm, setMailForm] = useState<MailServiceProviderRequest>({
    enabled: true,
  });
  const { data } = useQuery({
    queryKey: ["mailProvider", id],
    queryFn: async () => (await getDetail(id)).data,
    enabled: !!id,
    refetchOnMount: true,
    gcTime: 0,
  });
  const { content } = new Domain();
  const { data: domainData } = useQuery({
    queryKey: ["domain"],
    queryFn: async () => (await content({ target: "ID" })).data,
    // refetchOnMount: true,
    // refetchOnWindowFocus: true,
  });
  useEffect(() => {
    if (data) {
      setMailForm({ ...data, domainId: data?.domain?.id });
    }
  }, [data]);
  const queryClient = useQueryClient();
  const submit = () => {
    if (type === "update") {
      const submitData = {
        domainId: mailForm?.domainId,
        provider: mailForm?.provider,
        username: mailForm?.username,
        password: mailForm?.password,
        host: mailForm?.host,
        port: mailForm?.port,
        priority: mailForm?.priority,
        enabled: mailForm?.enabled,
        sender: mailForm?.sender,
      };
      update(id, submitData!).then(() => queryClient.invalidateQueries());
    } else {
      const submitData = {
        domainId: mailForm?.domainId ?? domainData?.[0].id,
        provider: mailForm?.provider ?? "AWS",
        username: mailForm?.username,
        password: mailForm?.password,
        host: mailForm?.host,
        port: mailForm?.port,
        priority: mailForm?.priority,
        enabled: mailForm?.enabled,
        sender: mailForm?.sender,
      };
      save(submitData!).then(() => queryClient.invalidateQueries());
    }
  };
  return (
    <Dialog.Content
      style={{
        padding: "44px 42px",
        paddingRight: "45px",
        width: "500px",
        height: "752px",
      }}
      onCloseAutoFocus={() => {
        setId && setId("");
      }}
    >
      <Flex justify={"end"}>
        <Dialog.Close>
          <IconButton variant="ghost" radius="full" color="gray">
            <Cross2Icon width="18" height="18" fill="black" />
          </IconButton>
        </Dialog.Close>
      </Flex>
      <Dialog.Title>
        {type === "create"
          ? "메일 서비스 제공자 생성"
          : "메일 서비스 제공자 상세"}
      </Dialog.Title>
      <Flex
        direction="column"
        gap="4"
        // style={{ maxHeight: "532px", overflowY: "auto", overflowX: "hidden" }}
      >
        {type === "update" && (
          <label>
            <Text as="div" size="1" mb="1" weight="medium">
              ID
            </Text>
            <TextField.Root
              defaultValue={data?.id}
              onChange={(ev) =>
                setMailForm((prev) => ({
                  ...prev,
                  domainId: ev.target.value,
                }))
              }
              placeholder="아이디 입력"
              style={{
                width: "408px",
                height: "48px",
                background: "#F5F6F8",
                padding: "16px",
              }}
              disabled={type === "update"}
            />
          </label>
        )}
        <Flex style={{ gap: "20px" }}>
          <label>
            <Text as="div" size="1" mb="1" weight="medium">
              도메인
            </Text>
            <Flex direction="column" width="194px">
              {type === "update" ? (
                data?.domain?.id && (
                  <Select.Root
                    defaultValue={data?.domain?.id}
                    size="3"
                    onValueChange={(value) => {
                      setMailForm((prev) => ({
                        ...prev,
                        domainId: value,
                      }));
                    }}
                  >
                    <Select.Trigger radius="medium" />
                    <Select.Content>
                      {domainData &&
                        domainData?.map((domain) => (
                          <Select.Item value={domain.id ?? ""}>
                            {domain.name}
                          </Select.Item>
                        ))}
                    </Select.Content>
                  </Select.Root>
                )
              ) : (
                <Select.Root
                  defaultValue={domainData?.[0]?.id}
                  size="3"
                  onValueChange={(value) => {
                    setMailForm((prev) => ({
                      ...prev,
                      domainId: value,
                    }));
                  }}
                >
                  <Select.Trigger radius="medium" />
                  <Select.Content>
                    {domainData?.map((domain) => (
                      <Select.Item value={domain.id ?? ""}>
                        {domain.name}
                      </Select.Item>
                    ))}
                  </Select.Content>
                </Select.Root>
              )}
            </Flex>
          </label>
          <label>
            <Text as="div" size="1" mb="1" weight="medium">
              제공자
            </Text>
            <Flex direction="column" width="194px">
              {type === "update" ? (
                data && (
                  <Select.Root
                    defaultValue={data?.provider as "AWS" | "OCI"}
                    size="3"
                    onValueChange={(value) =>
                      setMailForm((prev) => ({
                        ...prev,
                        provider: value as "AWS" | "OCI",
                        // provider: value.toUpperCase() as "AWS" | "OCI",
                      }))
                    }
                  >
                    <Select.Trigger radius="medium" />
                    <Select.Content>
                      <Select.Item value="AWS">AWS</Select.Item>
                      <Select.Item value="OCI">OCI</Select.Item>
                    </Select.Content>
                  </Select.Root>
                )
              ) : (
                <Select.Root
                  defaultValue={"AWS"}
                  size="3"
                  onValueChange={(value) =>
                    setMailForm((prev) => ({
                      ...prev,
                      provider: value as "AWS" | "OCI",
                    }))
                  }
                >
                  <Select.Trigger radius="medium" />
                  <Select.Content>
                    <Select.Item value="AWS">AWS</Select.Item>
                    <Select.Item value="OCI">OCI</Select.Item>
                  </Select.Content>
                </Select.Root>
              )}
            </Flex>
          </label>
        </Flex>
        <label>
          <Text as="div" size="1" mb="1" weight="medium">
            User name
          </Text>
          <TextField.Root
            placeholder="사용자 이름 입력"
            defaultValue={data && data?.username}
            onChange={(ev) =>
              setMailForm((prev) => ({
                ...prev,
                username: ev.target.value,
              }))
            }
            style={{
              width: "408px",
              height: "48px",
              background: "#F5F6F8",
              padding: "16px",
            }}
          />
        </label>
        <label>
          <Text as="div" size="1" mb="1" weight="medium">
            Password
          </Text>
          <TextField.Root
            onChange={(ev) =>
              setMailForm((prev) => ({
                ...prev,
                password: ev.target.value,
              }))
            }
            defaultValue={data?.password}
            placeholder="비밀번호 입력"
            style={{
              width: "408px",
              height: "48px",
              background: "#F5F6F8",
              padding: "16px",
            }}
          />
        </label>
        <label>
          <Text as="div" size="1" mb="1" weight="medium">
            Host
          </Text>
          <TextField.Root
            onChange={(ev) =>
              setMailForm((prev) => ({
                ...prev,
                host: ev.target.value,
              }))
            }
            defaultValue={data?.host}
            placeholder="주소 입력"
            style={{
              width: "408px",
              height: "48px",
              background: "#F5F6F8",
              padding: "16px",
            }}
          />
        </label>
        <label>
          <Flex style={{ gap: "20px" }}>
            <Flex direction={"column"}>
              <Text as="div" size="1" mb="1" weight="medium">
                Port
              </Text>
              <TextField.Root
                onChange={(ev) =>
                  setMailForm((prev) => ({
                    ...prev,
                    port: ev.target.value,
                  }))
                }
                defaultValue={data?.port}
                placeholder="포트 입력"
                style={{
                  width: "194px",
                  height: "48px",
                  background: "#F5F6F8",
                  padding: "16px",
                }}
              />
            </Flex>
            <Flex direction={"column"}>
              <Text as="div" size="1" mb="1" weight="medium">
                우선순위
              </Text>
              <TextField.Root
                type="number"
                defaultValue={data?.priority}
                onChange={(ev) =>
                  setMailForm((prev) => ({
                    ...prev,
                    priority: Number(ev.target.value),
                  }))
                }
                placeholder="순위 입력"
                style={{
                  width: "194px",
                  height: "48px",
                  background: "#F5F6F8",
                  padding: "16px",
                }}
              />
            </Flex>
          </Flex>
        </label>
        <label>
          <Text as="div" size="1" mb="1" weight="medium">
            발신자
          </Text>
          <RadioGroup.Root
            value={mailForm?.enabled ? "active" : "inactive"}
            name="활성상태"
            color="tomato"
            onValueChange={(value) => {
              setMailForm((prev) => ({
                ...prev,
                enabled: value === "active",
              }));
            }}
          >
            <Flex direction="row" style={{ gap: "136px" }}>
              <RadioGroup.Item value="active">활성</RadioGroup.Item>
              <RadioGroup.Item value="inactive">비활성</RadioGroup.Item>
            </Flex>
          </RadioGroup.Root>
        </label>
        <label>
          <Text as="div" size="1" mb="1" weight="medium">
            발신자
          </Text>
          <TextField.Root
            defaultValue={data?.sender}
            placeholder="발신자 이름 입력"
            onChange={(ev) =>
              setMailForm((prev) => ({
                ...prev,
                sender: ev.target.value,
              }))
            }
            style={{
              width: "408px",
              height: "48px",
              background: "#F5F6F8",
              padding: "16px",
            }}
          />
        </label>
      </Flex>
      <Flex gap="3" mt="7" justify="center">
        <Dialog.Close>
          <Button
            onClick={submit}
            style={{
              background: "#F53217",
              padding: "8px 16px 8px 16px",
              width: "100px",
              height: "40px",
            }}
          >
            저장
          </Button>
        </Dialog.Close>
      </Flex>
    </Dialog.Content>
  );
}
