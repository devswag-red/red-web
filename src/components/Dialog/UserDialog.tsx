import { Cross2Icon } from "@radix-ui/react-icons";
import {
  Flex,
  Text,
  Dialog,
  TextField,
  Button,
  RadioGroup,
  IconButton,
} from "@radix-ui/themes";
import { User } from "../../api/User";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { useEffect, useState } from "react";
import { USER_ROLE } from "../../util/enum";

export default function UserDialog({
  id,
  setId,
  isOpen,
  setIsOpen,
}: {
  id?: string;
  setId?: React.Dispatch<React.SetStateAction<string>>;
  isOpen: boolean;
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
}) {
  const [username, setUsername] = useState("");
  const [name, setName] = useState("");
  const [role, setRole] = useState("1");

  const userQuery = User.getInstance();
  const queryClient = useQueryClient();

  const { data, isLoading } = useQuery({
    queryKey: ["GET_USER_DETAIL" + id],
    queryFn: () => userQuery.getUserDetail(id as string),
    enabled: !!id,
  });

  useEffect(() => {
    if (data && !isLoading) {
      setUsername(data.data.username);
      setName(data.data.name);
      setRole(
        data.data.authorities[0].authority === USER_ROLE.ADMIN ? "1" : "2",
      );
    }
  }, [data, isLoading, id]);

  const handleClose = () => {
    setId && setId("");
    setRole("1");
    setName("");
    setUsername("");
    setIsOpen(false);
  };

  const saveMutation = useMutation({
    mutationFn: (data: { role_code: string; name: string; username: string }) =>
      userQuery.createNewUser({ ...data, role_code: Number(data.role_code) }),
    onSuccess: () => {
      queryClient.invalidateQueries(); // 도메인 목록을 갱신하기 위해 invalidate
      handleClose(); // 저장 성공 시 다이얼로그 닫기
    },
    onError: (res) => {
      const temp = { ...res };
      const msg = (temp as any).response.data.message;

      alert("아이디는" + msg.split(":")[1]);
    },
  });

  const updateMutation = useMutation({
    mutationFn: (data: { role_code: string; name: string; username: string }) =>
      userQuery.updateUser(id as string, {
        username: data.username,
        role_code: Number(data.role_code),
        name: data.name,
      }),
    onSuccess: () => {
      queryClient.invalidateQueries(); // 도메인 목록을 갱신하기 위해 invalidate
      handleClose(); // 수정 성공 시 다이얼로그 닫기
    },
    onError: (res) => {
      const temp = { ...res };
      const msg = (temp as any).response.data.message;

      alert(msg);
    },
  });

  const handleSave = () => {
    if (!name || !username) {
      alert("아이디와 이름을 모두 입력해 주세요");
      return;
    }

    const query = {
      username,
      name: name,
      role_code: role,
    };

    if (id) {
      updateMutation.mutate(query);
    } else {
      saveMutation.mutate(query);
    }
  };

  return (
    <Dialog.Root open={isOpen}>
      <Dialog.Content
        style={{ padding: "44px 42px", width: "492px" }}
        onCloseAutoFocus={handleClose}
      >
        <Flex justify={"end"}>
          <IconButton
            variant="ghost"
            radius="full"
            color="gray"
            onClick={() => setIsOpen(false)}
          >
            <Cross2Icon width="18" height="18" fill="black" />
          </IconButton>
        </Flex>
        <Dialog.Title>사용자 {id ? "상세" : "생성"}</Dialog.Title>
        <Flex direction="column" gap="4">
          <label>
            <Text as="div" size="1" mb="1" weight="medium">
              아이디
            </Text>
            <TextField.Root
              disabled={!!id}
              placeholder="아이디 입력"
              value={username}
              type="email"
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                setUsername(e.target.value)
              }
              style={{
                width: "408px",
                height: "48px",
                background: "#F5F6F8",
                padding: "16px",
              }}
            />
          </label>
          <label>
            <Text as="div" size="1" mb="1" weight="medium">
              이름
            </Text>
            <TextField.Root
              placeholder="사용자 이름 입력"
              value={name}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                setName(e.target.value)
              }
              style={{
                width: "408px",
                height: "48px",
                background: "#F5F6F8",
                padding: "16px",
              }}
            />
          </label>
          <label>
            <Text as="div" size="1" mb="1" weight="medium">
              권한
            </Text>

            <RadioGroup.Root
              name="role"
              color="tomato"
              value={role}
              onValueChange={(value) => setRole(value)}
              style={{ display: "flex" }}
            >
              <Flex direction="row" style={{ gap: "136px" }}>
                <RadioGroup.Item value={"1"}>관리자</RadioGroup.Item>
                <RadioGroup.Item value={"2"}>사용자</RadioGroup.Item>
              </Flex>
            </RadioGroup.Root>
          </label>
        </Flex>

        <Flex gap="3" mt="7" justify="center">
          <Button
            style={{
              background: "#F53217",
              padding: "8px 16px 8px 16px",
              width: "100px",
              height: "40px",
            }}
            onClick={handleSave}
          >
            저장
          </Button>
        </Flex>
      </Dialog.Content>
    </Dialog.Root>
  );
}
