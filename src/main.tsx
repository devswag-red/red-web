import ReactDOM from "react-dom/client";
import { RouterProvider, createBrowserRouter } from "react-router-dom";

import "./index.css";
import Router from "./Router";
import { Theme } from "@radix-ui/themes";
import "@radix-ui/themes/styles.css";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      refetchOnMount: false,
      networkMode: "always",
      retry: false,
    },
    mutations: { networkMode: "always" },
  },
});
ReactDOM.createRoot(document.getElementById("root")!).render(
  <QueryClientProvider client={queryClient}>
    <RouterProvider
      router={createBrowserRouter([
        {
          id: "MainRouter",
          path: "*",
          element: (
            <Theme>
              <Router />
            </Theme>
          ),
        },
      ])}
    />
  </QueryClientProvider>
);
