import styled from "@emotion/styled";
import { PropsWithChildren } from "react";
import { Header } from "../components/Header";

export default function Layout({ children }: PropsWithChildren) {
  return (
    <Wrapper>
      <Header />
      {children}
    </Wrapper>
  );
}

export const Wrapper = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
  background-color: #f5f6f8;
  overflow: auto;
  padding: 26px 28px 48px 20px;
`;
