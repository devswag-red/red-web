import styled from "@emotion/styled";
import {
  Badge,
  Box,
  Button,
  Flex,
  Select,
  Table,
  TextField,
} from "@radix-ui/themes";
import { MailLog } from "../../api/MailLog";
import { useQuery } from "@tanstack/react-query";
import { useState } from "react";
import { DatePicker } from "antd";
import dayjs, { Dayjs } from "dayjs";
import { nanoid } from "nanoid";

const TABLE_HEADER = [
  "생성일",
  "도메인",
  "제공자",
  "발신자",
  "수신자",
  "발송 상태",
  "에러 메시지",
];

export default function MailHistoryPage() {
  const [keyword, setKeyword] = useState("");
  const [sendStatus, setSendStatus] = useState<"ALL" | "FAIL" | "SUCCESS">(
    "ALL"
  );
  const [target, setTarget] = useState<
    "SENDER" | "RECEIVER" | "DOMAIN_NAME" | undefined
  >("SENDER");
  const [dateRange, setDateRange] = useState<[dayjs.Dayjs, dayjs.Dayjs]>([
    dayjs().startOf("d").add(-1, "week"),
    dayjs().endOf("d"),
  ]);

  const { getList2 } = MailLog.getInstance();

  const { data: mailLogData, refetch } = useQuery({
    queryKey: ["mailLog"],
    queryFn: async () =>
      (
        await getList2({
          startTime: dateRange[0].valueOf(),
          endTime: dateRange[1].valueOf(),
          target,
          keyword,
          sendStatus,
        })
      ).data,
  });

  const handleDateChange = (dates: [Dayjs | null, Dayjs | null] | null) => {
    if (dates && dates[0] && dates[1]) {
      setDateRange([dates[0], dates[1]]);
    }
  };

  const handleSearch = () => {
    refetch();
  };

  return (
    <div style={{ display: "flex", gap: "28px", flexDirection: "column" }}>
      <Title>메일 발송 이력</Title>
      <Filter>
        <Flex
          direction="row"
          p={"1"}
          width={"100%"}
          justify={"between"}
          style={{ gap: "30px" }}
        >
          <DatePicker.RangePicker
            value={dateRange}
            onChange={handleDateChange}
          />
          <Flex
            justify={"center"}
            align={"center"}
            gap={"10px"}
            style={{ overflow: "auto" }}
          >
            <div>검색어</div>
            <Select.Root
              value={target}
              onValueChange={(value: "SENDER" | "RECEIVER" | "DOMAIN_NAME") =>
                setTarget(value)
              }
              size="3"
            >
              <Select.Trigger radius="small" />
              <Select.Content style={{ width: "194px" }}>
                <Select.Item value="RECEIVER">수신자</Select.Item>
                <Select.Item value="SENDER">발신자</Select.Item>
                <Select.Item value="DOMAIN_NAME">도메인</Select.Item>
              </Select.Content>
            </Select.Root>
            <Box
              style={{
                width: "302px",
                height: "44px",
                display: "flex",
                alignItems: "center",
              }}
            >
              <TextField.Root
                placeholder="검색어 입력"
                size={"3"}
                style={{ width: "100%" }}
                value={keyword}
                onChange={(ev) => setKeyword(ev.target.value)}
              >
                <TextField.Slot>
                  {/* <MagnifyingGlassIcon height="16" width="16" /> */}
                </TextField.Slot>
              </TextField.Root>
            </Box>
          </Flex>
          <Flex justify={"center"} align={"center"} gap={"10px"}>
            <div>발송상태</div>
            <Select.Root
              value={sendStatus}
              onValueChange={(value: "ALL" | "FAIL" | "SUCCESS") =>
                setSendStatus(value)
              }
              size="3"
            >
              <Select.Trigger radius="none" />
              <Select.Content>
                <Select.Item value="ALL">전체</Select.Item>
                <Select.Item value="SUCCESS">성공</Select.Item>
                <Select.Item value="FAIL">실패</Select.Item>
              </Select.Content>
            </Select.Root>
          </Flex>
          <Button
            style={{
              width: "140px",
              height: "40px",
              padding: "10px 24px 10px 24px",
              borderRadius: "4px",
              background: "#F53217",
            }}
            onClick={handleSearch}
          >
            검색
          </Button>
        </Flex>
      </Filter>
      <Table.Root
        style={{
          background: "white",
          overflow: "auto",
          height: "calc(100vh - 450px)",
        }}
      >
        <Table.Header>
          <Table.Row>
            {TABLE_HEADER.map((header) => (
              <Table.ColumnHeaderCell
                key={nanoid()}
                style={{ whiteSpace: "nowrap" }}
              >
                {header}
              </Table.ColumnHeaderCell>
            ))}
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {mailLogData?.map((log) => (
            <Table.Row key={nanoid()}>
              <Table.RowHeaderCell style={{ whiteSpace: "nowrap" }}>
                {dayjs(log.createdTime).format("YYYY-MM-DD")}
              </Table.RowHeaderCell>
              <Table.Cell>{log.domainName}</Table.Cell>
              <Table.Cell>{log.providerType}</Table.Cell>
              <Table.Cell>{log.sender}</Table.Cell>
              <Table.Cell>{log.receiver}</Table.Cell>
              <Table.Cell>
                {log.success ? (
                  <Badge color="green">성공</Badge>
                ) : (
                  <Badge color="red">실패</Badge>
                )}
              </Table.Cell>
              <Table.Cell>
                {log.failMessage &&
                  log.failCode &&
                  log.failMessage + " " + log.failCode}
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table.Root>
    </div>
  );
}
const Title = styled.div`
  font-size: 24px;
  font-style: normal;
  font-weight: 600;
`;

const Filter = styled.div`
  border-radius: 16px;
  /* display: flex; */
  /* justify-content: center; */
  /* align-items: center; */

  width: 100%;
  padding: 28px;
  height: 100px;
  box-sizing: border-box;
  flex-shrink: 0;
  background: var(--Gray_25, #f5f6f8);
`;
