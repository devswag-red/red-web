import styled from "@emotion/styled";
import {
  Box,
  Button,
  Dialog,
  Flex,
  Select,
  Table,
  TextField,
} from "@radix-ui/themes";
import MailServiceProviderDialog from "../../components/Dialog/MailServiceProviderDialog";
import { MailServiceProvider } from "../../api/MailServiceProvider";
import { useQuery } from "@tanstack/react-query";
import { useEffect, useRef, useState } from "react";
import { MailServiceProviderListRequest } from "../../api/data-contracts";

const TABLE_HEADER = [
  "ID",
  "도메인",
  "제공자",
  "우선순위",
  "User name",
  "Password",
];

export default function MailHistoryPage() {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedID, setSelectedID] = useState("");
  const { getList1 } = new MailServiceProvider();

  const inputRef = useRef<HTMLInputElement>(null);

  const { data: mailHistoryData, refetch } = useQuery({
    queryKey: ["mailHistory"],
    queryFn: async () =>
      (
        await getList1({
          keyword: inputRef.current?.value ?? "",
          searchTarget: searchOption,
        })
      ).data,
    refetchOnMount: true,
    refetchOnWindowFocus: true,
    gcTime: 0,
  });

  useEffect(() => {
    refetch();
  }, [isOpen]);

  const [searchOption, setSearchOption] =
    useState<MailServiceProviderListRequest["searchTarget"]>("DOMAIN");
  return (
    <div style={{ display: "flex", gap: "28px", flexDirection: "column" }}>
      <Title>메일 서비스 제공자 (AWS/OCI)</Title>
      <Filter>
        <Flex align={"center"} style={{ overflow: "auto" }}>
          <Flex
            direction="row"
            p={"1"}
            width={"100%"}
            gap={"3"}
            align={"center"}
          >
            <Select.Root
              defaultValue="ID"
              size="3"
              onValueChange={(value) =>
                setSearchOption(
                  value as MailServiceProviderListRequest["searchTarget"]
                )
              }
            >
              <Select.Trigger radius="medium" />
              <Select.Content>
                <Select.Item value="ID">ID</Select.Item>
                <Select.Item value="DOMAIN">도메인</Select.Item>
                <Select.Item value="PROVIDER">제공자</Select.Item>
                <Select.Item value="USERNAME">유저이름</Select.Item>
              </Select.Content>
            </Select.Root>
            <Box style={{ width: "540px" }}>
              <TextField.Root
                ref={inputRef}
                placeholder="검색어 입력"
                size={"3"}
              >
                <TextField.Slot></TextField.Slot>
              </TextField.Root>
            </Box>
            <Button
              onClick={() => refetch()}
              style={{
                width: "140px",
                height: "40px",
                padding: "10px 24px 10px 24px",
                borderRadius: "4px",
                background: "#F53217",
              }}
            >
              검색
            </Button>
          </Flex>
          <Dialog.Root onOpenChange={(open) => setIsOpen(open)}>
            <Dialog.Trigger>
              <Button
                style={{
                  color: "white",
                  background: "black",
                  width: "140px",
                  height: "40px",
                  padding: "10px 24px 10px 24px",
                }}
              >
                생성
              </Button>
            </Dialog.Trigger>
            <MailServiceProviderDialog type="create" id="" />
          </Dialog.Root>
        </Flex>
      </Filter>
      <Table.Root
        style={{
          background: "white",
          overflow: "auto",
          height: "calc(100vh - 450px)",
        }}
      >
        <Table.Header>
          <Table.Row>
            {TABLE_HEADER.map((header) => (
              <Table.ColumnHeaderCell style={{ whiteSpace: "nowrap" }}>
                {header}
              </Table.ColumnHeaderCell>
            ))}
          </Table.Row>
        </Table.Header>
        <Table.Body>
          <Dialog.Root onOpenChange={(open) => setIsOpen(open)}>
            {mailHistoryData?.map((history) => (
              <Dialog.Trigger onClick={() => setSelectedID(history?.id ?? "")}>
                <Table.Row>
                  <ShortenRowHeaderCell style={{ whiteSpace: "nowrap" }}>
                    {history.id}
                  </ShortenRowHeaderCell>
                  <ShortenTableCell>{history.domain?.name}</ShortenTableCell>
                  <ShortenTableCell>{history.provider}</ShortenTableCell>
                  <ShortenTableCell>{history.priority}</ShortenTableCell>
                  <ShortenTableCell>{history.username}</ShortenTableCell>
                  <ShortenTableCell>{history.password}</ShortenTableCell>
                </Table.Row>
              </Dialog.Trigger>
            ))}
            <MailServiceProviderDialog
              type="update"
              id={selectedID || ""}
              setId={setSelectedID}
            />
          </Dialog.Root>
        </Table.Body>
      </Table.Root>
    </div>
  );
}

const Title = styled.div`
  font-size: 24px;
  font-style: normal;
  font-weight: 600;
`;

const Filter = styled.div`
  border-radius: 16px;
  /* display: flex; */
  /* justify-content: center; */
  /* align-items: center; */

  width: 100%;
  padding: 28px;
  height: 100px;
  box-sizing: border-box;
  flex-shrink: 0;
  background: var(--Gray_25, #f5f6f8);
`;

const ShortenTableCell = styled(Table.Cell)`
  max-width: 100px;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  word-break: break-all;
`;

const ShortenRowHeaderCell = styled(Table.RowHeaderCell)`
  max-width: 100px;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  word-break: break-all;
`;
