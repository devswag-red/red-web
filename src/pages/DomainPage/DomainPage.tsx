import styled from "@emotion/styled";
import {
  Box,
  Button,
  Dialog,
  Flex,
  Select,
  Table,
  TextField,
} from "@radix-ui/themes";
import DomainDialog from "../../components/Dialog/DomainDialog";
import { useQuery } from "@tanstack/react-query";
import { Domain } from "../../api/Domain";
import dayjs from "dayjs";
import { useState } from "react";

const TABLE_HEADER = ["ID", "도메인", "생성일"];

export default function DomainPage() {
  const [id, setId] = useState("");
  const [keyword, setKeyword] = useState("");
  const [searchType, setSearchType] = useState<"ID" | "NAME">("ID");
  const { content } = Domain.getInstance();

  const {
    data: DomainListData,
    isLoading,
    refetch,
  } = useQuery({
    queryKey: ["GET_DOMAINS"],
    queryFn: async () =>
      (
        await content({
          target: searchType,
          keyword: keyword,
        })
      ).data,
  });

  if (isLoading) {
    return <>로딩</>;
  }

  return (
    <div style={{ display: "flex", gap: "28px", flexDirection: "column" }}>
      <Title>도메인</Title>
      <Filter>
        <Flex align={"center"}>
          <Flex
            direction="row"
            p={"1"}
            width={"100%"}
            gap={"3"}
            align={"center"}
          >
            <Select.Root
              defaultValue="ID"
              size="3"
              onValueChange={(value: "ID" | "NAME") => setSearchType(value)}
            >
              <Select.Trigger radius="medium" />
              <Select.Content>
                <Select.Item value="ID">ID</Select.Item>
                <Select.Item value="NAME">NAME</Select.Item>
              </Select.Content>
            </Select.Root>
            <Box
              style={{
                width: "540px",
              }}
            >
              <TextField.Root
                placeholder="검색어 입력"
                size={"3"}
                onChange={(ev) => setKeyword(ev.target.value)}
              >
                <TextField.Slot>
                  {/* <MagnifyingGlassIcon height="16" width="16" /> */}
                </TextField.Slot>
              </TextField.Root>
            </Box>
            <Button
              style={{
                width: "140px",
                height: "40px",
                padding: "10px 24px 10px 24px",
                borderRadius: "4px",
                background: "#F53217",
              }}
              onClick={() => refetch()}
            >
              검색
            </Button>
          </Flex>
          <Dialog.Root>
            <Dialog.Trigger>
              <Button
                style={{
                  color: "white",
                  background: "black",
                  width: "140px",
                  height: "40px",
                  padding: "10px 24px 10px 24px",
                }}
              >
                생성
              </Button>
            </Dialog.Trigger>
            <DomainDialog type="create" />
          </Dialog.Root>
        </Flex>

        {/* </div> */}
      </Filter>
      <Table.Root style={{ background: "white" }}>
        <Table.Header>
          <Table.Row>
            {TABLE_HEADER.map((header) => (
              <Table.ColumnHeaderCell>{header}</Table.ColumnHeaderCell>
            ))}
          </Table.Row>
        </Table.Header>

        <Table.Body>
          <Dialog.Root>
            {DomainListData?.map((item) => {
              return (
                <Dialog.Trigger onClick={() => setId(item.id as string)}>
                  <Table.Row>
                    <Table.RowHeaderCell>{item.id}</Table.RowHeaderCell>
                    <Table.Cell>{item.name}</Table.Cell>
                    <Table.Cell>
                      {dayjs(item.createdTime).format("YYYY-MM-DD")}
                    </Table.Cell>
                  </Table.Row>
                </Dialog.Trigger>
              );
            })}
            <DomainDialog type="update" id={id} />
          </Dialog.Root>
        </Table.Body>
      </Table.Root>
    </div>
  );
}

const Title = styled.div`
  font-size: 24px;
  font-style: normal;
  font-weight: 600;
`;

const Filter = styled.div`
  border-radius: 16px;
  /* display: flex; */
  /* justify-content: center; */
  /* align-items: center; */

  width: 100%;
  padding: 28px;
  height: 100px;
  box-sizing: border-box;
  flex-shrink: 0;
  background: var(--Gray_25, #f5f6f8);
`;
