import styled from "@emotion/styled";
import {
  Box,
  Button,
  Dialog,
  Flex,
  Select,
  Table,
  TextField,
} from "@radix-ui/themes";
import ClientDialog from "../../components/Dialog/ClientDialog";
import { Client } from "../../api/Client";
import { useQuery } from "@tanstack/react-query";
import { useEffect, useRef, useState } from "react";
import dayjs from "dayjs";
import { ClientRequest } from "../../api/data-contracts";

const TABLE_HEADER = ["ID", "도메인", "Secret", "생성일"];

export default function ClientPage() {
  const [selectedID, setSelectedID] = useState("");
  const [isOpen, setIsOpen] = useState(false);
  const [target, setTarget] = useState<ClientRequest["target"]>("ID");
  const { content1 } = new Client();
  const inputRef = useRef<HTMLInputElement>(null);
  const { data: clientList, refetch } = useQuery({
    queryKey: ["mailHistory"],
    queryFn: async () =>
      (
        await content1({
          keyword: inputRef.current?.value ?? "",
          target: target,
        })
      ).data,
    refetchOnMount: true,
    refetchOnWindowFocus: true,
  });
  useEffect(() => {
    refetch();
    console.log(":::isOpen", isOpen);
  }, [isOpen]);

  return (
    <div
      style={{
        display: "flex",
        gap: "28px",
        flexDirection: "column",
      }}
    >
      <Title>클라이언트</Title>
      <Filter>
        <Flex align={"center"} style={{ overflow: "auto" }}>
          <Flex
            direction="row"
            p={"1"}
            width={"100%"}
            gap={"3"}
            align={"center"}
          >
            <Select.Root
              defaultValue="ID"
              size="3"
              value={target}
              onValueChange={(value: "ID" | "DOMAIN" | "SECRET") =>
                setTarget(value)
              }
            >
              <Select.Trigger radius="medium" />
              <Select.Content>
                <Select.Item value="ID">ID</Select.Item>
                <Select.Item value="DOMAIN">DOMAIN</Select.Item>
                <Select.Item value="SECRET">SECRET</Select.Item>
              </Select.Content>
            </Select.Root>
            <Box style={{ width: "540px" }}>
              <TextField.Root
                placeholder="검색어 입력"
                size={"3"}
                ref={inputRef}
              >
                <TextField.Slot></TextField.Slot>
              </TextField.Root>
            </Box>
            <Button
              onClick={() => refetch()}
              style={{
                width: "140px",
                height: "40px",
                padding: "10px 24px 10px 24px",
                borderRadius: "4px",
                background: "#F53217",
              }}
            >
              검색
            </Button>
          </Flex>
          <Dialog.Root onOpenChange={(open) => setIsOpen(open)}>
            <Dialog.Trigger>
              <Button
                style={{
                  color: "white",
                  background: "black",
                  width: "140px",
                  height: "40px",
                  padding: "10px 24px 10px 24px",
                }}
              >
                생성
              </Button>
            </Dialog.Trigger>
            <ClientDialog type="create" id="" />
          </Dialog.Root>
        </Flex>
      </Filter>
      <Table.Root
        style={{
          background: "white",
          overflow: "auto",
          height: "calc(100vh - 450px)",
        }}
      >
        <Table.Header>
          <Table.Row>
            {TABLE_HEADER.map((header) => (
              <Table.ColumnHeaderCell>{header}</Table.ColumnHeaderCell>
            ))}
          </Table.Row>
        </Table.Header>

        <Table.Body>
          <Dialog.Root>
            {clientList?.map((client) => (
              <Dialog.Trigger onClick={() => setSelectedID(client?.id ?? "")}>
                <Table.Row>
                  <Table.RowHeaderCell>{client.id}</Table.RowHeaderCell>
                  <Table.Cell>{client.domainName}</Table.Cell>
                  <Table.Cell>{client.secret}</Table.Cell>
                  <Table.Cell>
                    {dayjs(client.createdTime).format("YYYY-MM-DD")}
                  </Table.Cell>
                </Table.Row>
              </Dialog.Trigger>
            ))}
            <ClientDialog type="update" id={selectedID} setId={setSelectedID} />
          </Dialog.Root>
        </Table.Body>
      </Table.Root>
    </div>
  );
}

const Title = styled.div`
  font-size: 24px;
  font-style: normal;
  font-weight: 600;
`;

const Filter = styled.div`
  border-radius: 16px;
  /* display: flex; */
  /* justify-content: center; */
  /* align-items: center; */

  width: 100%;
  padding: 28px;
  height: 100px;
  box-sizing: border-box;
  flex-shrink: 0;
  background: var(--Gray_25, #f5f6f8);
`;
