import styled from "@emotion/styled";
import { Flex } from "@radix-ui/themes";

const LoginWrapper = styled(Flex)`
  display: flex;
  justify-content: center;
  min-width: 100vw;
  min-height: 100vh;
  opacity: 1;
  background-color: #f5f6f8;
`;

const LoginBoxStyle = styled.div`
  display: flex;
  justify-content: center;
  width: 528px;
  height: 552px;
  flex-shrink: 0;
  border-radius: 24px;
  opacity: 1;
  background: #fff;
  margin-top: 20vh;
`;

export { LoginBoxStyle, LoginWrapper };
