import RegisterForm from "../../../components/LoginForm/RegisterForm";
import * as S from "../commonStyle";

export default function RegisterPage() {
  return (
    <S.LoginWrapper>
      <S.LoginBoxStyle style={{ height: "612px" }}>
        <RegisterForm />
      </S.LoginBoxStyle>
    </S.LoginWrapper>
  );
}
