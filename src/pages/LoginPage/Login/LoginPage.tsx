import LoginForm from "../../../components/LoginForm/LoginForm";
import * as S from "../commonStyle";

export default function LoginPage() {
  return (
    <S.LoginWrapper>
      <S.LoginBoxStyle>
        <LoginForm />
      </S.LoginBoxStyle>
    </S.LoginWrapper>
  );
}
