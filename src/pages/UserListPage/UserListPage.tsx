import styled from "@emotion/styled";
import {
  Badge,
  Box,
  Button,
  Flex,
  Select,
  Table,
  TextField,
  Dialog,
} from "@radix-ui/themes";
import UserDialog from "../../components/Dialog/UserDialog";
import { nanoid } from "nanoid";
import { User } from "../../api/User";
import { useQuery } from "@tanstack/react-query";
import { USER_ROLE } from "../../util/enum";
import { useState } from "react";
import { IUser } from "../../api/data-contracts";
import dayjs from "dayjs";

export default function UserListPage() {
  const [id, setId] = useState<string>("");
  const [dialog, setDialog] = useState(false);
  const userListQuery = User.getInstance();
  const TABLE_HEADER = ["사용자 ID", "이름", "권한", "생성일"];

  const { data: userList, isLoading } = useQuery({
    queryKey: ["GET_USER_LIST"],
    queryFn: () =>
      userListQuery.getUserList({
        page: 1,
        size: 9999,
        sort: "id",
      }),
  });

  if (isLoading) {
    return <>로딩</>;
  }

  return (
    <div style={{ display: "flex", gap: "28px", flexDirection: "column" }}>
      <Title>사용자 목록</Title>
      <Filter>
        <Flex style={{ overflow: "auto" }}>
          <Flex
            direction="row"
            p={"1"}
            width={"100%"}
            gap={"3"}
            align={"center"}
          >
            <Select.Root defaultValue="userID" size="3">
              <Select.Trigger radius="medium" />
              <Select.Content>
                <Select.Item value="userID">사용자 ID</Select.Item>
              </Select.Content>
            </Select.Root>
            <Box style={{ width: "540px" }}>
              <TextField.Root placeholder="검색어 입력" size={"3"}>
                <TextField.Slot>
                  {/* <MagnifyingGlassIcon height="16" width="16" /> */}
                </TextField.Slot>
              </TextField.Root>
            </Box>
            <Button
              style={{
                width: "140px",
                height: "40px",
                padding: "10px 24px 10px 24px",
                borderRadius: "4px",
                background: "#F53217",
              }}
            >
              검색
            </Button>
          </Flex>
          <Flex align={"center"}>
            <Button
              style={{
                color: "white",
                background: "black",
                width: "140px",
                height: "40px",
                padding: "10px 24px 10px 24px",
              }}
              onClick={() => setDialog(true)}
            >
              사용자 생성
            </Button>
            <UserDialog isOpen={dialog} setIsOpen={setDialog} />
          </Flex>
        </Flex>
        {/* </div> */}
      </Filter>

      <Table.Root
        style={{
          background: "white",
          overflow: "auto",
          height: "calc(100vh - 450px)",
        }}
      >
        <Table.Header>
          <Table.Row>
            {TABLE_HEADER.map((header) => (
              <Table.ColumnHeaderCell key={nanoid()}>
                {header}
              </Table.ColumnHeaderCell>
            ))}
          </Table.Row>
        </Table.Header>

        <Table.Body>
          <Dialog.Root>
            {userList?.data &&
              (userList?.data as IUser[]).map((item) => {
                return (
                  <Dialog.Trigger
                    key={item.id}
                    onClick={() => {
                      setDialog(true);
                      setId(item.id);
                    }}
                    style={{ cursor: "pointer" }}
                  >
                    <Table.Row>
                      <Table.RowHeaderCell>{item.username}</Table.RowHeaderCell>
                      <Table.Cell>{item.name}</Table.Cell>
                      <Table.Cell>
                        {item.authorities[0].authority === USER_ROLE.USER ? (
                          <Badge color="green">사용자</Badge>
                        ) : (
                          <Badge color="blue">관리자</Badge>
                        )}
                      </Table.Cell>
                      <Table.Cell>
                        {item.createdAt &&
                          dayjs(item.createdAt).format("YYYY-MM-DD")}
                      </Table.Cell>
                    </Table.Row>
                  </Dialog.Trigger>
                );
              })}
            <UserDialog
              id={id}
              setId={setId}
              isOpen={dialog}
              setIsOpen={setDialog}
            />
          </Dialog.Root>
        </Table.Body>
      </Table.Root>
    </div>
  );
}

const Title = styled.div`
  font-size: 24px;
  font-style: normal;
  font-weight: 600;
`;

const Filter = styled.div`
  border-radius: 16px;
  /* display: flex; */
  /* justify-content: center; */
  /* align-items: center; */

  width: 100%;
  padding: 28px;
  height: 100px;
  box-sizing: border-box;
  flex-shrink: 0;
  background: var(--Gray_25, #f5f6f8);
`;
