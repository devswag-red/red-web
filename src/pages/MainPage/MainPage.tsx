import styled from "@emotion/styled";
import Sidebar from "../../components/Sidebar";
import { ISideMenu } from "../../types/types";
import { nanoid } from "nanoid";
import MailIcon from "../../components/icon/MailIcon";
import UserListIcon from "../../components/icon/UserListIcon";
import DomainIcon from "../../components/icon/DomainIcon";
import ClientIcon from "../../components/icon/ClientIcon";
import { Navigate, Route, Routes } from "react-router-dom";
import UserListPage from "../UserListPage/UserListPage";
import { PATHS } from "../../util/errorCode";
import MailHistoryPage from "../MailHistoryPage/MailHistoryPage";
import DomainPage from "../DomainPage/DomainPage";
import ClientPage from "../ClientPage/ClientPage";
import ProviderPage from "../ProviderPage/ProviderPage";
import ProviderIcon from "../../components/icon/ProviderIcon";

const menuList: ISideMenu[] = [
  {
    title: "사용자 목록",
    icon: UserListIcon,
    id: nanoid(),
    path: PATHS.userList,
  },
  {
    title: "메일 발송 이력",
    icon: MailIcon,
    id: nanoid(),
    path: PATHS.mailHistory,
  },
  {
    title: "도메인",
    icon: DomainIcon,
    id: nanoid(),
    path: PATHS.domain,
  },
  {
    title: "클라이언트",
    icon: ClientIcon,
    id: nanoid(),
    path: PATHS.client,
  },
  {
    title: "메일 서비스 제공자",
    icon: ProviderIcon,
    id: nanoid(),
    path: PATHS.mailServiceProvider,
  },
];

export default function MainPage() {
  return (
    <MainContainer>
      <Sidebar menuList={menuList} />
      <Content>
        <Routes>
          <Route path="/" element={<Navigate to={PATHS.userList} replace />} />
          <Route path={PATHS.userList} element={<UserListPage />} />
          <Route path={PATHS.mailHistory} element={<MailHistoryPage />} />
          <Route path={PATHS.domain} element={<DomainPage />} />
          <Route path={PATHS.client} element={<ClientPage />} />
          <Route path={PATHS.mailServiceProvider} element={<ProviderPage />} />
        </Routes>
      </Content>
    </MainContainer>
  );
}

const MainContainer = styled.div`
  display: flex;
  height: 100%;
  padding-top: 32px;
`;

const Content = styled.div`
  flex: 1;
  background-color: white;
  border-radius: 28px;
  padding: 26px 32px;
  /* padding: 16px; */
`;
