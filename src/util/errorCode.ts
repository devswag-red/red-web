export const ERROR_CODE = {
  NOT_MATCH_PASSWORD: "NOT_MATCH_PASSWORD", // 비밀번호 확인이 잘못됨
  INVALID_CREDENTIALS: "INVALID_CREDENTIALS", // 아이디와 패스워드가 잘못됨
};

export const PATHS = {
  main: "/",
  login: "/login",
  register: "/register",
  component: "/component",
  userList: "/user-list",
  client: "/client",
  domain: "/domain",
  mailHistory: "/mail-history",
  mailServiceProvider: "/mail-provider",
  notFound: "*",
};
