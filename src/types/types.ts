import { ElementType } from "react";

export interface ISideMenu {
  title: string;
  icon: ElementType;
  id: string;
  path: string;
}

export interface IconProps {
  fill?: string;
  width?: number;
  height?: number;
}
