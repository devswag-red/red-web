/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import {
  CreateNewUserData,
  CreateUserRequest,
  DeleteUserData,
  DeleteUserRequest,
  GetUserDetailData,
  GetUserListData,
  Pageable,
  UpdatePasswordData,
  UpdatePasswordRequest,
  UpdateUserData,
  UpdateUserRequest,
} from "./data-contracts";
import { ContentType, HttpClient, RequestParams } from "./http-client";

export class User<
  SecurityDataType = unknown,
> extends HttpClient<SecurityDataType> {
  private static instance: User;

  private constructor() {
    super();
  }

  public static getInstance(): User {
    if (!User.instance) {
      User.instance = new User();
    }
    return User.instance;
  }
  /**
   * @description 사용자 정보 조회
   *
   * @tags 사용자 관리
   * @name GetUserDetail
   * @summary 사용자 정보 조회
   * @request GET:/user/{id}
   * @response `200` `GetUserDetailData` 성공
   */
  getUserDetail = (id: string, params: RequestParams = {}) =>
    this.request<GetUserDetailData, any>({
      path: `/user/${id}`,
      method: "GET",
      ...params,
    });
  /**
   * @description 사용자 수정
   *
   * @tags 사용자 관리
   * @name UpdateUser
   * @summary 사용자 수정
   * @request PUT:/user/{id}
   * @response `200` `UpdateUserData` 성공
   */
  updateUser = (
    id: string,
    data: UpdateUserRequest,
    params: RequestParams = {},
  ) =>
    this.request<UpdateUserData, any>({
      path: `/user/${id}`,
      method: "PUT",
      body: data,
      type: ContentType.Json,
      ...params,
    });
  /**
   * @description 비밀번호 수정
   *
   * @tags 사용자 관리
   * @name UpdatePassword
   * @summary 비밀번호 수정
   * @request PUT:/user/password
   * @response `200` `UpdatePasswordData` 성공
   */
  updatePassword = (data: UpdatePasswordRequest, params: RequestParams = {}) =>
    this.request<UpdatePasswordData, any>({
      path: `/user/password`,
      method: "PUT",
      body: data,
      type: ContentType.Json,
      ...params,
    });
  /**
   * @description 사용자 생성
   *
   * @tags 사용자 관리
   * @name CreateNewUser
   * @summary 사용자 생성
   * @request POST:/user
   * @response `200` `CreateNewUserData` 성공
   */
  createNewUser = (data: CreateUserRequest, params: RequestParams = {}) =>
    this.request<CreateNewUserData, any>({
      path: `/user`,
      method: "POST",
      body: data,
      type: ContentType.Json,
      ...params,
    });
  /**
   * @description 사용자 삭제
   *
   * @tags 사용자 관리
   * @name DeleteUser
   * @summary 사용자 삭제
   * @request DELETE:/user
   * @response `200` `DeleteUserData` 성공
   */
  deleteUser = (data: DeleteUserRequest, params: RequestParams = {}) =>
    this.request<DeleteUserData, any>({
      path: `/user`,
      method: "DELETE",
      body: data,
      type: ContentType.Json,
      ...params,
    });
  /**
   * @description 사용자 목록 조회
   *
   * @tags 사용자 관리
   * @name GetUserList
   * @summary 사용자 목록 조회
   * @request GET:/user/list
   * @response `200` `GetUserListData` 성공
   */
  getUserList = (query: Pageable, params: RequestParams = {}) =>
    this.request<GetUserListData, any>({
      path: `/user`,
      method: "GET",
      query: {
        ...query,
        sort: "id",
      },
      ...params,
    });
}
