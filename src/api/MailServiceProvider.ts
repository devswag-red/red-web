/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import {
  GetDetailData,
  GetList1Data,
  MailServiceProviderListRequest,
  MailServiceProviderRequest,
  SaveData,
  UpdateData,
} from "./data-contracts";
import { ContentType, HttpClient, RequestParams } from "./http-client";

export class MailServiceProvider<
  SecurityDataType = unknown,
> extends HttpClient<SecurityDataType> {
  /**
   * No description
   *
   * @tags mail-service-provider-controller
   * @name GetDetail
   * @request GET:/mail-service-provider/{id}
   * @response `200` `GetDetailData` OK
   */
  getDetail = (id: string, params: RequestParams = {}) =>
    this.request<GetDetailData, any>({
      path: `/mail-service-provider/${id}`,
      method: "GET",
      ...params,
    });
  /**
   * No description
   *
   * @tags mail-service-provider-controller
   * @name Update
   * @request PUT:/mail-service-provider/{id}
   * @response `200` `UpdateData` OK
   */
  update = (
    id: string,
    data: MailServiceProviderRequest,
    params: RequestParams = {}
  ) =>
    this.request<UpdateData, any>({
      path: `/mail-service-provider/${id}`,
      method: "PUT",
      body: data,
      type: ContentType.Json,
      ...params,
    });
  /**
   * No description
   *
   * @tags mail-service-provider-controller
   * @name GetList1
   * @request GET:/mail-service-provider
   * @response `200` `GetList1Data` OK
   */
  getList1 = (
    query: MailServiceProviderListRequest,
    params: RequestParams = {}
  ) =>
    this.request<GetList1Data, any>({
      path: `/mail-service-provider`,
      method: "GET",
      query: query,
      ...params,
    });
  /**
   * No description
   *
   * @tags mail-service-provider-controller
   * @name Save
   * @request POST:/mail-service-provider
   * @response `200` `SaveData` OK
   */
  save = (data: MailServiceProviderRequest, params: RequestParams = {}) =>
    this.request<SaveData, any>({
      path: `/mail-service-provider`,
      method: "POST",
      body: data,
      type: ContentType.Json,
      ...params,
    });
}
