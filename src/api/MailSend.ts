/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import { GetListData, MailSendRequest } from "./data-contracts";
import { HttpClient, RequestParams } from "./http-client";

export class MailSend<SecurityDataType = unknown> extends HttpClient<SecurityDataType> {
  /**
   * No description
   *
   * @tags mail-send-controller
   * @name GetList
   * @request POST:/mail-send
   * @response `200` `GetListData` OK
   */
  getList = (
    query: {
      request: MailSendRequest;
    },
    params: RequestParams = {},
  ) =>
    this.request<GetListData, any>({
      path: `/mail-send`,
      method: "POST",
      query: query,
      ...params,
    });
}
