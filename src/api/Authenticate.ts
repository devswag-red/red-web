/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import { LogInData, LoginRequest } from "./data-contracts";
import { HttpClient, RequestParams } from "./http-client";

export class Authenticate<
  SecurityDataType = unknown,
> extends HttpClient<SecurityDataType> {
  private static instance: Authenticate;

  private constructor() {
    super();
  }

  public static getInstance(): Authenticate {
    if (!Authenticate.instance) {
      Authenticate.instance = new Authenticate();
    }
    return Authenticate.instance;
  }

  /**
   * No description
   *
   * @tags authentication-controller
   * @name LogIn
   * @request GET:/authenticate
   * @response `200` `LogInData` OK
   */
  logIn = (query: LoginRequest, params: RequestParams = {}) =>
    this.request<LogInData, any>({
      path: `/authenticate`,
      method: "GET",
      query: query,
      ...params,
    });
}
