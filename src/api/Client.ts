/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import {
  ClientCreateRequest,
  ClientRequest,
  ClientUpdateRequest,
  Content1Data,
  Detail1Data,
  Save2Data,
  Update2Data,
} from "./data-contracts";
import { ContentType, HttpClient, RequestParams } from "./http-client";

export class Client<
  SecurityDataType = unknown,
> extends HttpClient<SecurityDataType> {
  /**
   * No description
   *
   * @tags client-controller
   * @name Detail1
   * @request GET:/client/{id}
   * @response `200` `Detail1Data` OK
   */
  detail1 = (id: string, params: RequestParams = {}) =>
    this.request<Detail1Data, any>({
      path: `/client/${id}`,
      method: "GET",
      ...params,
    });
  /**
   * No description
   *
   * @tags client-controller
   * @name Update2
   * @request PUT:/client/{id}
   * @response `200` `Update2Data` OK
   */
  update2 = (
    id: string,
    data: ClientUpdateRequest,
    params: RequestParams = {}
  ) =>
    this.request<Update2Data, any>({
      path: `/client/${id}`,
      method: "PUT",
      body: data,
      type: ContentType.Json,
      ...params,
    });
  /**
   * No description
   *
   * @tags client-controller
   * @name Content1
   * @request GET:/client
   * @response `200` `Content1Data` OK
   */
  content1 = (query: ClientRequest, params: RequestParams = {}) =>
    this.request<Content1Data, any>({
      path: `/client`,
      method: "GET",
      query: query,
      ...params,
    });
  /**
   * No description
   *
   * @tags client-controller
   * @name Save2
   * @request POST:/client
   * @response `200` `Save2Data` OK
   */
  save2 = (data: ClientCreateRequest, params: RequestParams = {}) =>
    this.request<Save2Data, any>({
      path: `/client`,
      method: "POST",
      body: data,
      type: ContentType.Json,
      ...params,
    });
}
