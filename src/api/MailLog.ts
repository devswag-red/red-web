/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import { GetList2Data, MailSendLogRequest } from "./data-contracts";
import { HttpClient, RequestParams } from "./http-client";

export class MailLog<
  SecurityDataType = unknown,
> extends HttpClient<SecurityDataType> {
  private static instance: MailLog;

  private constructor() {
    super();
  }

  public static getInstance(): MailLog {
    if (!MailLog.instance) {
      MailLog.instance = new MailLog();
    }
    return MailLog.instance;
  }

  /**
   * No description
   *
   * @tags mail-send-log-controller
   * @name GetList2
   * @request GET:/mail-log
   * @response `200` `GetList2Data` OK
   */
  getList2 = (query: MailSendLogRequest, params: RequestParams = {}) =>
    this.request<GetList2Data, any>({
      path: `/mail-log`,
      method: "GET",
      query: query,
      ...params,
    });
}
