/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

import {
  ContentData,
  DetailData,
  DomainCreateRequest,
  DomainRequest,
  DomainUpdateRequest,
  Save1Data,
  Update1Data,
} from "./data-contracts";
import { ContentType, HttpClient, RequestParams } from "./http-client";

export class Domain<
  SecurityDataType = unknown,
> extends HttpClient<SecurityDataType> {
  private static instance: Domain;

  constructor() {
    super();
  }

  public static getInstance(): Domain {
    if (!Domain.instance) {
      Domain.instance = new Domain();
    }
    return Domain.instance;
  }

  /**
   * No description
   *
   * @tags domain-controller
   * @name Detail
   * @request GET:/domain/{id}
   * @response `200` `DetailData` OK
   */
  detail = (id: string, params: RequestParams = {}) =>
    this.request<DetailData, any>({
      path: `/domain/${id}`,
      method: "GET",
      ...params,
    });
  /**
   * No description
   *
   * @tags domain-controller
   * @name Update1
   * @request PUT:/domain/{id}
   * @response `200` `Update1Data` OK
   */
  update1 = (
    id: string,
    data: DomainUpdateRequest,
    params: RequestParams = {},
  ) =>
    this.request<Update1Data, any>({
      path: `/domain/${id}`,
      method: "PUT",
      body: data,
      type: ContentType.Json,
      ...params,
    });
  /**
   * No description
   *
   * @tags domain-controller
   * @name Content
   * @request GET:/domain
   * @response `200` `ContentData` OK
   */
  content = (query: DomainRequest, params: RequestParams = {}) =>
    this.request<ContentData, any>({
      path: `/domain`,
      method: "GET",
      query: query,
      ...params,
    });
  /**
   * No description
   *
   * @tags domain-controller
   * @name Save1
   * @request POST:/domain
   * @response `200` `Save1Data` OK
   */
  save1 = (data: DomainCreateRequest, params: RequestParams = {}) =>
    this.request<Save1Data, any>({
      path: `/domain`,
      method: "POST",
      body: data,
      type: ContentType.Json,
      ...params,
    });
}
