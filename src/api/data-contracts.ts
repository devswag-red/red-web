/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface UpdateUserRequest {
  username: string;
  /** @format int64 */
  role_code?: number;
  name: string;
}

export interface UpdatePasswordRequest {
  id: string;
  currentPassword: string;
  newPassword: string;
}

export interface MailServiceProviderRequest {
  domainId?: string;
  provider?: "AWS" | "OCI";
  username?: string;
  password?: string;
  host?: string;
  port?: string;
  /** @format int32 */
  priority?: number;
  enabled?: boolean;
  sender?: string;
}

export interface DomainUpdateRequest {
  name?: string;
}

export interface DomainResponse {
  id?: string;
  name?: string;
  /** @format int64 */
  createdTime?: number;
}

export interface ClientUpdateRequest {
  domainId?: string;
  enabled?: boolean;
}

export interface ClientResponse {
  id?: string;
  domainId?: string;
  domainName?: string;
  secret?: string;
  enabled?: boolean;
  /** @format int64 */
  createdTime?: number;
}

export interface CreateUserRequest {
  username: string;
  name: string;
  /** @format int64 */
  role_code?: number;
}

export interface IdResponse {
  id?: string;
}

export interface MailSendRequest {
  secretKey?: string;
  sender?: string;
  receiver?: string;
  subject?: string;
  text?: string;
}

export interface DomainCreateRequest {
  name?: string;
}

export interface ClientCreateRequest {
  domainId?: string;
  enabled?: boolean;
}

export interface Pageable {
  /**
   * @format int32
   * @min 0
   */
  page?: number;
  /**
   * @format int32
   * @min 1
   */
  size?: number;
  sort?: string;
}

export interface MailServiceProviderListRequest {
  searchTarget?: "ID" | "DOMAIN" | "PROVIDER" | "USERNAME";
  keyword?: string;
}

export interface MailServiceProviderResponse {
  id?: string;
  sender?: string;
  provider?: "AWS" | "OCI";
  username?: string;
  password?: string;
  host?: string;
  port?: string;
  /** @format int32 */
  priority?: number;
  enabled?: boolean;
  /** @format int64 */
  createdTime?: number;
  domain?: DomainResponse;
}

export interface MailSendLogRequest {
  /** @format int64 */
  startTime?: number;
  /** @format int64 */
  endTime?: number;
  target?: "SENDER" | "RECEIVER" | "DOMAIN_NAME";
  keyword?: string;
  sendStatus?: "ALL" | "SUCCESS" | "FAIL";
}

export interface MailSendLogResponse {
  /** @format int64 */
  createdTime?: number;
  domainName?: string;
  providerType?: string;
  sender?: string;
  receiver?: string;
  success?: boolean;
  failCode?: string;
  failMessage?: string;
}

export interface DomainRequest {
  target?: "ID" | "NAME";
  keyword?: string;
}

export interface ClientRequest {
  target?: "ID" | "DOMAIN" | "SECRET";
  keyword?: string;
}

export interface LoginRequest {
  username: string;
  password: string;
}

export interface DeleteUserRequest {
  targetId: string;
}

export interface IUser {
  id: string;
  username: string;
  name: string;
  createdAt: number;
  authorities: {
    authority: string;
  }[];
}

export type GetUserDetailData = IUser;

export type UpdateUserData = object;

export type UpdatePasswordData = object;

export type GetDetailData = MailServiceProviderResponse;

export type UpdateData = any;

export type DetailData = DomainResponse;

export type Update1Data = DomainResponse;

export type Detail1Data = ClientResponse;

export type Update2Data = ClientResponse;

export type CreateNewUserData = object;

export type DeleteUserData = object;

export type GetList1Data = MailServiceProviderResponse[];

export type SaveData = IdResponse;

export type GetListData = any;

export type ContentData = DomainResponse[];

export type Save1Data = DomainResponse;

export type Content1Data = ClientResponse[];

export type Save2Data = ClientResponse;

export type GetUserListData = object;

export type GetList2Data = MailSendLogResponse[];

export type LogInData = object;
